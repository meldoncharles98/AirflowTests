import pytest
from airflow.models import DagBag
from datetime import datetime, timedelta




class TestDagsValidation:
    LOAD_THRESHOLD = timedelta(seconds=2)
    EXPECTED_NUMBER_OF_DAGS = 1
    REQUIRED_EMAIL = "Dag_alerts@williamhill.us"

    def test_import_dags(self, dagbag):
        """
        Verify that Airflow is able to import all DAGs in the repo
        -- check for typos
        -- check for cycles etc
        :param dagbag:
        :return: bool
        """
        assert len(dagbag.import_errors) == 0, f"DAG import failures found! :{dagbag.import_errors}"

    def test_time_import_dags(self, dagbag):
        """
        verify that DAGs load fast enough. dagbag_stats has duration of load
        :param dagbag:
        :return: bool
        """
        stats = dagbag.dagbag_stats
        slow_dags = list(filter(lambda f: f.duration > self.LOAD_THRESHOLD, stats))
        dag_list = map(lambda f: f.file[1:], slow_dags)
        dag_list = ",".join(dag_list)

        assert len(
            slow_dags) == 0, f"The following dags take more than {self.LOAD_THRESHOLD} seconds to load: {dag_list}"

    def test_number_of_dags(self, dagbag):
        """
            Verify if there is the right number of DAGs in the dag folder
            - Check number of dags
        """
        stats = dagbag.dagbag_stats
        dag_num = sum([o.dag_num for o in stats])
        assert dag_num == self.EXPECTED_NUMBER_OF_DAGS, "Wrong number of dags, {0} expected got {1} (Can be due to cycles!)".format(
            self.EXPECTED_NUMBER_OF_DAGS, dag_num)

    @pytest.mark.skip(reason="Not yet implemented")
    def test_default_args_email(self, dagbag):
        """
        Verify that a defaul alert email has been specified for a DAG
        - Check email
        :param dagbag:
        :return: bool
        """
        for dag_id, dag in dagbag.dags.items():
            emails = dag.default_args.get('email', [])
            assert self.REQUIRED_EMAIL in emails, f"Missing required alert email {self.REQUIRED_EMAIL} in dag {dag_id}"

    def test_default_args_retries(self, dagbag):
        """
        Verify DAGs have the required number of retries
        - Check retries
        :param dagbag:
        :return: bool
        """
        for dag_id, dag in dagbag.dags.items():
            retries = dag.default_args.get('retries', None)
            assert retries is not None, f"Retries not specified for tasks in dags {dag_id}"

    def test_default_arg_retry_delay(self, dagbag):
        """
        Verfiy that retry_delay has been implemented at the DAG level in seconds
        --check rety_delay
        :param dagbag:
        :return: bool
        """
        for dag_id, dag in dagbag.dags.items():
            retry_delay = dag.default_args.get('retry_delay', None)
            assert retry_delay is not None, f"Retry delay has to be specified in seconds for {dag_id}"
