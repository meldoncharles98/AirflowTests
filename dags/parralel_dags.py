from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.subdag_operator import SubDagOperator
from airflow.utils.task_group import TaskGroup
from datetime import datetime, timedelta

default_args = {
    'start_date': datetime(2020, 1, 1)
}

with DAG('parallel_dag',
         schedule_interval='@daily',
         default_args=default_args,
         catchup=False
         ) as dag:
    task1 = BashOperator(
        task_id='task_1',
        bash_command='sleep 3'
    )

    #use TaskGroup instead of subgroups... much better doesnt cause deadlocks, use up all slots, use sequential executor, slower
    with TaskGroup('processing_tasks') as tg:
        task2 = BashOperator(
            task_id='task_2',
            bash_command='sleep 3'
        )
        #can have a taskgroup inside of a taskGroup .. can have similar named taskids but the nested inner task will be Dataflow_jobtask.task_3
        with TaskGroup('Dataflow_jobtask') as Dataflow_task:

            task3 = BashOperator(
                task_id='task_3',
                bash_command='echo "Dataflow on spark" && sleep 3'
               # bash_command='sleep 3'
            )
        with TaskGroup('Beam_tasks') as Beam_task:

            task3 = BashOperator(
                task_id='task_3',
                bash_command='echo "Beam in Go on Dataflow" && sleep 3'
            )

    task4 = BashOperator(
        task_id='task_4',
        bash_command='sleep 4'
    )
    task1 >> tg >> task4
