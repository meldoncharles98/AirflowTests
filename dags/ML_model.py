from airflow import DAG
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.operators.bash import BashOperator
from random import randint
from datetime import datetime, timedelta


def _training_model():
    return randint(1, 10)


def _choosing_best_model(ti):
    accuracies = ti.xcom_pull(
        task_ids=[
            f'training_model_{model_id}' for model_id in ['A', 'B', 'C']
        ])
    if max(accuracies) > 8:
        return 'accurate'
    else:
        return 'inaccurate'


default_args = {
    'owner': 'Airflow',
    'retries': 5,
    'retry_delay': timedelta(seconds=4),

}

with DAG("My_Dag",
         default_args=default_args,
         start_date=datetime(2021, 4, 1),
         schedule_interval='@daily',
         catchup=True,

         ) as dag:
    training_model_task = [
        PythonOperator(
            task_id=f"training_model_{model_id}",

            python_callable=_training_model,
            op_kwargs={
                "model": model_id
            }
        ) for model_id in ['A', 'B', 'C']
    ]
    choosing_best_model = BranchPythonOperator(
        task_id="choosing_best_model",
        python_callable=_choosing_best_model
    )

    accurate = BashOperator(
        task_id="accurate",
        bash_command=" echo 'accurate'"
    )
    inaccurate = BashOperator(
        task_id="inaccurate",
        bash_command=" echo 'inaccurate'"
    )
    training_model_task >> choosing_best_model >> [accurate, inaccurate] \
        # >>training_model_task
